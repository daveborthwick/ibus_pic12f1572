FlySky iBus to PPM Converter based on PIC12F1572 Hardware. Useful if you have patched your i6 transmitter for 8 channels.

Input is the iBus stream from an iA6B receiver. May also work with other receivers but I haven'tried it. Outputs an 8 channel PPM stream to go into flight controller. Also outputs PWM channels for channel 7 and 8 to use with the 6 from the receiver if you want 8 channels of PWM.

More details will follow when I get around to it, but for the time being 
for those who are used to PIC development, here is the method.

* Download and install Microchips MPLabs X and XC8 Compiler
* Create a new project selecting PIC12F1572
* Only one source file is needed main.c
* Compile
* Download to your chip. I use PicKit 3.

Circuit is very simple hardly justifies a schematic ...

* 1    VDD         +5V (If driving from RC Receiver make sure does not exceed 5.5V))
* 2    UART RX     iBus input
* 3    UART TX     Not Used
* 4    MCLR        Only used for programming
* 5    RA2         8 Channel PPM Output
* 6    RA1         PWM Ch8 Output + Programming
* 7    RA0         PWM Ch7 Output + Programming
* 8    VSS         0V

100nF decoupling capacitor between pins 1 & 8

You can flatten out the pins of the chip and solder wires direct to them or make a mini pcb. Encapsulate with some heat shrink sleaving. With an MSOP chip and a fine soldering iron it can be tiny.


![ibus2ppm.jpg](https://bitbucket.org/repo/B6jKdb/images/2406713253-ibus2ppm.jpg)