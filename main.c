// iBus to PPM converter for PIC 12F1572
//
// Copyright Dave Borthwick 2015
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//    
//    

#include <xc.h>
#include <stdint.h>

// PINS
// 1    VDD         +5V (If driving from RC Receiver make sure does not exceed 5.5V))
// 2    UART RX     iBus input
// 3    UART TX     Not Used
// 4    MCLR        Only used for programming
// 5    RA2         PPM Output
// 6    RA1         PWM Ch8 Output
// 7    RA0         PWM Ch7 Output
// 8    VSS         0V

// CONFIG1
#pragma config BOREN = ON    // Brown-out Reset Enable->Brown-out Reset enabled
#pragma config PWRTE = OFF    // Power-up Timer Enable->PWRT disabled
#pragma config FOSC = INTOSC    // Oscillator Selection->INTOSC oscillator: I/O function on CLKIN pin
#pragma config MCLRE = ON    // MCLR Pin Function Select->MCLR/VPP pin function is MCLR
#pragma config CP = OFF    // Flash Program Memory Code Protection->Program memory code protection is disabled
#pragma config WDTE = OFF    // Watchdog Timer Enable->WDT disabled
#pragma config CLKOUTEN = OFF    // Clock Out Enable->CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin

// CONFIG2
#pragma config WRT = OFF    // Flash Memory Self-Write Protection->Write protection off
#pragma config LVP = OFF    // Low-Voltage Programming Enable->High-voltage on MCLR/VPP must be used for programming
#pragma config STVREN = ON    // Stack Overflow/Underflow Reset Enable->Stack Overflow or Underflow will cause a Reset
#pragma config PLLEN = ON    // PLL Enable->4x PLL enabled
#pragma config BORV = LO    // Brown-out Reset Voltage Selection->Brown-out Reset Voltage (Vbor), low trip point selected.

#define _XTAL_FREQ  32000000

// FlySky iBus
// -----------
#define IBUS_BUFFSIZE 33			// biggest iBus message seen so far + 1
#define IBUS_CMD_CHANNELS 0x40		// Command indicates that a string of RC channels follow

// iBus channel message consists of
// byte 0				length of packet (including this byte) normally 0x20
// byte 1				command = 0x40
// byte 2,3				RC channel 1 = 256 * byte3 + byte2 (little endian)
// byte 4,5				RC channel 2
// ...					Further RC channels
// byte len-2, len-1	Checksum = 0xFFFF - sum( all previous bytes) little endian

uint8_t ibusFrame[IBUS_BUFFSIZE];	// ibus frame receive buffer
uint8_t ibusFrameCount;             // Count of bytes received for current ibus frame
uint8_t ibusGapDetect;              // used to detect time between frames
                                    // set to 1 everytime a byte is received
                                    // set to 0 every timer 0
                                    // if it is still 0 after a timer 0 period then 
                                    // something has gone wrong so restart frame
uint8_t validIbusReceived;          // Set when ever a valid iBus Frame has been 
                                    // received, cleared by PPM frame being sent

// Combined PPM
// ------------
#define PPM_CHANNELS 10			// Number of channels in the frame

// ppm signal repeats at the frame rate 20ms.
// Idle level is low.
// Each frame consists of a number of channels.
// Each channels consists of 400us of high
// followed by the remainder to make up the channel timing
// which can be 500 - 1700us making a total of
// 900us - 2100us per channel.
 
uint8_t ppmTimerH[PPM_CHANNELS];	// ppm timer high byte
uint8_t ppmTimerL[PPM_CHANNELS];	// ppm timer low byte
uint8_t ppmChannel;                 // channel to be played out

#define PPM_PIN RA2
#define PPM_MIN 900     // smallest value allowed -120%
#define PPM_MAX 2100    // largest value allowed +120%

// PWM
// ---
// The extra two channels 7 & 8 not normally available on
// an i6 receiver are presented as PWM on two GPIO pins

#define PWM

#ifdef PWM
#define CH7_PIN RA0
#define CH8_PIN RA1
#endif        

// Timers
// ------

// Timer 0 serves two purposes 
// Every time it expires is used to check for inter iBus frames
// Every 14 times it expires is used for start of a PPM frame
#if PPM_CHANNELS==8
#define TMR0_INTERRUPT_TICKER_FACTOR    14
#elif PPM_CHANNELS==10
#define TMR0_INTERRUPT_TICKER_FACTOR    18
#else
#error Unsupported number of channels
#endif



#define TMR0_RELOAD 0xD2     // 1472uS
uint8_t tmr0PostScaler;      // Post Scaler Counter for timer 0

// Timer 1 is used to measure the main PPM channel times. 
// This needs to be as accurate as possible so is corrected
// using a value found by trial and error.
#define TMR1_CALIBRATION 39  // Offset to add to allow for instruction cycles

// Timer 2 is used for the 400us part of each PPM pulse
// It doesn't have to be totally accurate as the important timing
// comes from timer 1
#define TMR2_RELOAD 0xC7     // 400uS

// ----------------------------------------------------------------------
/*
                        Interrupt handlers
 */

void PpmFrameStart(void)
{
    // this code executes every 14 TMR0 periods
    ppmChannel = 0;
    
    // Start Timer 1
    T1CONbits.TMR1ON = 1;
     
     // PPM Frame starts
    // Output goes high
    PPM_PIN = 1;

    // Start Timer 1 at rate based on channel 1
    TMR1H = ppmTimerH[ppmChannel];
    TMR1L = ppmTimerL[ppmChannel];

        
    // Start Timer 2 at 400us
    PR2 = TMR2_RELOAD;
    T2CONbits.TMR2ON = 1;
    
    ppmChannel = 1;
}

void interrupt INTERRUPT_InterruptManager(void) {
    // interrupt handler
    if (PIR1bits.TMR1IF == 1)
    {
        // Timer 1 - End of Timed pulse
        // output goes high
        PPM_PIN=1;
        
        // Restart timer 1 with new channel value;
        TMR1H = ppmTimerH[ppmChannel];
        TMR1L = ppmTimerL[ppmChannel];
        
        // Restart timer 2
        PR2 = TMR2_RELOAD;
        T2CONbits.TMR2ON = 1;
        
#ifdef PWM
        // Stop/Start the PWM channels if needed
        if (ppmChannel == 6)
        {
            //Start of CH7
            CH7_PIN = 1;           
        }
        if (ppmChannel == 7)
        {
            //Start of CH7
            CH7_PIN = 0;           
        }

        
        if (ppmChannel == 7)
        {
            // Start of CH8
            CH8_PIN = 1;
      
        }
        if (ppmChannel == 8)
        {
            // End of CH8
            CH8_PIN = 0;
        }
#endif        
        // increment channel and stop if reached the end
        ppmChannel++;
        if (ppmChannel > PPM_CHANNELS)
        {
            //stop timer
           T1CONbits.TMR1ON = 0;           
        }
       
        
        // Clear the TMR1 interrupt flag
        PIR1bits.TMR1IF = 0;


    }
    else if (PIR1bits.TMR2IF == 1)
    {
        // Timer 2 - End of 400us pulse
        // output goes low
        PPM_PIN=0;

        // Clear the TMR2 interrupt flag
        PIR1bits.TMR2IF = 0;

        //stop timer
        T2CONbits.TMR2ON = 0;
    }
    else if (INTCONbits.TMR0IF == 1)
    {
        // Timer 0 - Frame Rate pulse
        // clear the TMR0 interrupt flag
        INTCONbits.TMR0IF = 0;

        TMR0 = TMR0_RELOAD;

        // callback function - called every 8th pass
        if (++tmr0PostScaler >= TMR0_INTERRUPT_TICKER_FACTOR) {
            // ticker function call
            if ( validIbusReceived )
            {
                validIbusReceived = 0;
                PpmFrameStart();               
            }


            // reset ticker counter
            tmr0PostScaler = 0;
        }

        // check for gap between ibus frames
        // If timer expires and we haven't had a byte then
        // restart the frame.
        if (ibusGapDetect == 0)
        {
            // restart a frame
            ibusFrameCount = 0;
        }
        ibusGapDetect = 0;
    }
}

// ----------------------------------------------------------------------
/*
                         Main application
 */
void main(void) {
    
    // =====================
    // initialize the device
    // =====================
    
    // Oscillator
    // ----------
    // SPLLEN disabled; SCS FOSC; IRCF 8MHz_HF; 
    OSCCON = 0x70;
    // OSTS intosc; HFIOFR disabled; HFIOFS not0.5percent_acc; PLLR disabled; T1OSCR disabled; MFIOFR disabled; HFIOFL not2percent_acc; LFIOFR disabled; 
    OSCSTAT = 0x00;
    // TUN 0x0; 
    OSCTUNE = 0x00;
 
    // Wait for PLL to stabilize
    while (PLLR == 0) {
    }
    
    // GPIO Pins
    // ---------
    LATA = 0x00;
    TRISA = 0x38;
    ANSELA = 0x00;
    WPUA = 0x00;

    OPTION_REGbits.nWPUEN = 0x01;

    APFCON = 0x84;

    // USART
    // -----

    // ABDEN disabled; WUE disabled; RCIDL idle; ABDOVF no_overflow; SCKP async_noninverted_sync_fallingedge; BRG16 16bit_generator; 
    BAUDCON = 0x48;

    // ADDEN disabled; RX9 8-bit; RX9D 0x0; FERR no_error; CREN enabled; SPEN enabled; SREN disabled; OERR no_error; 
    RCSTA = 0x90;

    // CSRC slave_mode; TRMT TSR_empty; TXEN disabled; BRGH hi_speed; SYNC asynchronous; SENDB sync_break_complete; TX9D 0x0; TX9 8-bit; 
    TXSTA = 0x06;

    // Baud Rate = 115200; SPBRGL 68; 
    SPBRGL = 0x44;

    // Baud Rate = 115200; SPBRGH 0; 
    SPBRGH = 0x00;
   
    // Timer 0
    // -------
        // PSA assigned; PS 1:256; TMRSE Increment_hi_lo; mask the nWPUEN and INTEDG bits
    OPTION_REG = (OPTION_REG & 0xC0) | 0xD7 & 0x3F;

    TMR0 = TMR0_RELOAD;

    // Clear Interrupt flag before enabling the interrupt
    INTCONbits.TMR0IF = 0;

    // Enabling TMR0 interrupt
    INTCONbits.TMR0IE = 1;
    
    // Timer 1
    // -------
    //T1OSCEN disabled; nT1SYNC synchronize; T1CKPS 1:1; TMR1CS FOSC/4; TMR1ON disabled; 
    T1CON = 0x00;

    //T1GVAL disabled; T1GSPM disabled; T1GSS T1G; T1GTM disabled; T1GPOL low; TMR1GE disabled; T1GGO done; 
    T1GCON = 0x00;

    // Clearing IF flag before enabling the interrupt.
    PIR1bits.TMR1IF = 0;

    // Enabling TMR1 interrupt.
    PIE1bits.TMR1IE = 1;
    
    // Timer 2
    // -------
    // TMR2ON off; T2CKPS 1:16; T2OUTPS 1:1; 
    T2CON = 0x02;

    // TMR2 0x0; 
    TMR2 = 0x00;

    // Clearing IF flag before enabling the interrupt.
    PIR1bits.TMR2IF = 0;

    // Enabling TMR2 interrupt.
    PIE1bits.TMR2IE = 1;

    

    // ==========================
    // initialize the application
    // ==========================

    PPM_PIN = 0;
#ifdef PWM
    CH7_PIN = 0;
    CH8_PIN = 0;
#endif    
    tmr0PostScaler = 0;
    validIbusReceived = 0;
    
    // ===
    // RUN
    // ===
    
    // Enable the Global Interrupts
    INTCONbits.GIE = 1;

    // Enable the Peripheral Interrupts
    INTCONbits.PEIE = 1;

    while (1)
    {
        while (!PIR1bits.RCIF) {
            }
       
        if (1 == RCSTAbits.OERR) {
            // EUSART error - restart

            RCSTAbits.CREN = 0;
            RCSTAbits.CREN = 1;
        }

        // read byte from the USART
        ibusFrame[ibusFrameCount] = RCREG;

        ibusGapDetect = 1;
        if (ibusFrameCount < IBUS_BUFFSIZE-1)
           ibusFrameCount++;       

        // Idle processing - take the data from the ibus buffer and populate 
        // the ppm frame arrays
        
        // Error conditions requiring restart of frame
        if ((ibusFrameCount > 0 && ibusFrame[0] > IBUS_BUFFSIZE-1) ||
            (ibusFrameCount > 0 && ibusFrame[0] < 4 ) ||
            (ibusFrameCount > 0 && ibusFrame[0] %2 == 1 ) ||
            (ibusFrameCount > 1 && ibusFrame[1] != IBUS_CMD_CHANNELS))
        {
            ibusFrameCount = 0;
        }
        
        // Full frame -c check it is good
        else if (ibusFrameCount > 3 && ibusFrameCount >= ibusFrame[0] )
        {
            
            // Complete frame - calculate checksum
            // checksum is 0xFFFF - sum of all bytes except last 
            // two. These last two are the little endian checksum
            uint16_t calc_chksum = 0xFFFF;
            for (uint8_t i = 0; i < ibusFrame[0] - 2 ; i++)
            {
                calc_chksum -= ibusFrame[i];
            }
           
            if (calc_chksum == (ibusFrame[ibusFrame[0]-1] <<8) + ibusFrame[ibusFrame[0]-2])
            {
                uint16_t val;
                // good checksum so transfer the channel data
                for (uint8_t i = 0; (i < (ibusFrame[0] - 4)/2) && (i < PPM_CHANNELS) ; i++)
                {
                    // extract value in us
                    val = (ibusFrame[2+(i<<1)] + (ibusFrame[3+(i<<1)]<<8)) ;
                    if (val < PPM_MIN)
                        val = PPM_MIN;
                    if (val > PPM_MAX)
                        val = PPM_MAX;
                    
                    // convert to timer values
                    val = TMR1_CALIBRATION + ~(val<<3);
                    ppmTimerL[i] = val & 0xFF;
                    ppmTimerH[i] = val>>8;
               }
                validIbusReceived = 1;
            }
            
            // reset the frame regardless if it is a good one
            // or it failed the checksum
            ibusFrameCount = 0;
            
        }
    }
}